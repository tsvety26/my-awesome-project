module.exports = {
  platform: 'gitlab',
  endpoint: 'https://gitlab.acme.com/api/v4/',
  token: 'glpat-SZYL6jduf3Xydr5kgst_',

  repositories: [
    'acme/amber-js',
  ],

  logLevel: 'info',

  requireConfig: true,
  onboarding: true,
  onboardingConfig: {
    extends: ['config:base'],
    prConcurrentLimit: 5,
  },

  enabledManagers: [
    'npm',
  ],
};
